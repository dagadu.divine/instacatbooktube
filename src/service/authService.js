import axios from "axios";

async function authenticate(apiKey) {
  try {
    axios.defaults.headers.common["x-api-key"] = `${apiKey}`;
    //   axios.CancelToken = new axios.CancelToken;
    return axios
      .get("https://api.thecatapi.com/v1/images")
      .then((res) => {
        return res.status;
      })
      .catch((err) => {
        throw err;
      });
  } catch (e) {}

}

export default { authenticate };
