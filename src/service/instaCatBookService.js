import axios from "axios";

async function getAllImages(apiKey, limit) {
  try {
    axios.defaults.headers.common["x-api-key"] = `${apiKey}`;

    const data = axios.get(
      `https://api.thecatapi.com/v1/images/search?limit=${limit}`
    );

    return data;
  } catch (e) {}
}

async function saveFavorite(apiKey, payload) {
  try {
    axios.defaults.headers.common["x-api-key"] = `${apiKey}`;

    const data = axios.post(`https://api.thecatapi.com/v1/favourites`, payload);

    console.log(data, "data");
    return data;
  } catch (e) {}
}

async function deleteFavorite(apiKey, id) {
  try {
    axios.defaults.headers.common["x-api-key"] = `${apiKey}`;

    const data = axios.delete(`https://api.thecatapi.com/v1/favourites/${id}`);

    return data;
  } catch (e) {}
}

async function getAllFavorites(apiKey, limit) {
  try {
    axios.defaults.headers.common["x-api-key"] = `${apiKey}`;

    const data = axios.get(
      `https://api.thecatapi.com/v1/favourites?limit=${limit}`
    );

    return data;
  } catch (e) {}
}

export default { getAllImages, saveFavorite, deleteFavorite, getAllFavorites };
