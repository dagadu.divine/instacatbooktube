import logo from "./logo.svg";
import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import routes from "./routes";
import { NoMatch } from "./pages/";

//context
import { AuthContext, InstaCatBookContext } from "./context/";

function App() {
  return (
    <AuthContext.ProviderWrapper>
      <InstaCatBookContext.ProviderWrapper>
        <div className="App">
          <Routes>
            {routes.map(({ path, exact, component: Component, ...rest }) => {
              return (
                <Route
                  key={path}
                  path={path}
                  exact={exact}
                  element={<Component {...rest} />}
                />
              );
            })}
            <Route path="*" element={ <NoMatch />} />
          </Routes>
        </div>
      </InstaCatBookContext.ProviderWrapper>
    </AuthContext.ProviderWrapper>
  );
}

export default App;
