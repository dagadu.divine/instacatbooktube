import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { Exit, Favorites, Like } from "../components/svgs";
import { useNavigate } from "react-router-dom";

//hooks
import { useInstaCatBook } from "../hooks";

const FeedScreen = () => {
  const navRef = useRef(null);
  const contentRef = useRef(null);
  const imageRef = useRef(null);

  let navigate = useNavigate();

  // const [favourite, setFavourite] = useState(false);

  const [backgroundColor, setBackgroundColor] = useState("#fff");

  const { imagesData, saveFavorite, deleteFavorite, getAllImages } =
    useInstaCatBook();
  const [newImageData, setNewImageData] = useState([]);

  const apiKey = "a6c7f4d0-0b2e-44f0-8584-e1e9d6799732";

  useEffect(() => {
    if (imagesData.length > 0) {
      setNewImageData(imagesData);
    }
  }, [imagesData]);

  useEffect(() => {
    getAllImages();
    if (imagesData) {
      setNewImageData(imagesData);
    }
  }, []);

  const handleScroll = () => {
    if (contentRef.current?.scrollTop) {
      const backgroundcolor =
        contentRef.current?.scrollTop > 50 ? "#ffffff80" : "white";

      setBackgroundColor(backgroundcolor);
    }

    if (
      contentRef.current?.scrollHeight - contentRef.current?.scrollTop ===
      710
    ) {
      getAllImages();
    }
    // }
  };

  window.addEventListener("scroll", handleScroll, true);

  return (
    <>
      <Wrapper ref={contentRef}>
        <Navigation backgroundColor={backgroundColor} ref={navRef}>
          <Exit onClick={(e)=>{
            e.preventDefault();
            localStorage.clear()
            navigate("/");
          }}/>
          <NavText>instabooktube</NavText>
          <Favorites
            onClick={() => {
              navigate("/favorites");
            }}
          />
        </Navigation>
        <Content ref={imageRef}>
          <ContentInner>
            {newImageData !== [] &&
              newImageData?.map((image, index) => {
                // console.log(image.favorite, "image.favorite");
                return (
                  <ImageContainer key={index}>
                    <Image key={index} src={image?.url} alt="cat" />
                    <ImageOverlay
                      onClick={(e) => {
                        e.preventDefault();
                        const newResult = { ...newImageData[index] };
                        const newArray = newImageData.map((image, index) => {
                          if (image.id === newResult.id) {
                            return { ...image, favorite: !image.favorite };
                          }
                          return image;
                        });
                        setNewImageData([...newArray]);
                        if (newArray[index].favorite === true) {
                          // console.log(image.favorite, "favorite");
                          const payload = { image_id: image.id };
                          saveFavorite(apiKey, payload);
                        } else {
                          console.log(image.favorite, "DELETE");
                          deleteFavorite(apiKey, image.id);
                        }
                      }}
                    >
                      <Like liked={newImageData[index].favorite} />
                    </ImageOverlay>
                  </ImageContainer>
                );
              })}
          </ContentInner>
        </Content>
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  display: flex;
  width: 360px;
  height: 710px;
  flex-direction: column;
  overflow-y: scroll;
  overflow-x: hidden;
`;

const Navigation = styled.div`
  width: inherit;
  height: 8%;
  background-color: ${({ backgroundColor }) => backgroundColor};
  position: absolute;
  align-items: center;
  display: flex;
  justify-content: space-around;
  z-index: 4;
`;

const NavText = styled.p``;

const Content = styled.div`
  width: 100%;
  height: 90%;
  margin-top: 65px;
`;

const ContentInner = styled.div``;

const ImageContainer = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 95%;
  height: auto;
  /* position: absolute; */
`;

const ImageOverlay = styled.div`
  position: absolute;
  right: 20px;
  bottom: 50px;
  width: 34px;
  height: 34px;
  background-color: #62cc6d;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
`;

export default FeedScreen;
