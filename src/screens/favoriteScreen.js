import React, { Component, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { Back, Exit, Favorites, Like } from "../components/svgs";
import { useNavigate } from "react-router-dom";

//hooks
import { useInstaCatBook } from "../hooks";
import { keyframes } from "styled-components";
import { css } from "styled-components";

const FavoriteScreen = () => {
  const navRef = useRef(null);
  const contentRef = useRef(null);
  const imageRef = useRef([]);

  let navigate = useNavigate();

  const [backgroundColor, setBackgroundColor] = useState("#fff");
  const [startAnimation, setStartAnimation] = useState([]);

  const { getAllFavorites, deleteFavorite, favoriteData } = useInstaCatBook();

  const [newImageData, setNewImageData] = useState([]);

  const apiKey = "a6c7f4d0-0b2e-44f0-8584-e1e9d6799732";

  const [done, setDone] = useState(false);

  useEffect(() => {
    if (favoriteData.length > 0) {
      setNewImageData(favoriteData);
      setStartAnimation((prev) => {
        const res = favoriteData.map((_, i) => {
          return (startAnimation[i] = false);
        });
        return [...res];
      });
    }
  }, [favoriteData]);

  useEffect(() => {
    getAllFavorites();
    if (favoriteData) {
      setNewImageData(favoriteData);
    }
  }, []);

  const handleScroll = () => {
    setDone(false);
    if (contentRef.current?.scrollTop) {
      const backgroundcolor =
        contentRef.current?.scrollTop > 50 ? "#ffffff80" : "white";

      setBackgroundColor(backgroundcolor);
    }
    setDone(true);
  };

  const handleAnimation = (index) => {
    // const animated = !startAnimation[index];
    setStartAnimation((prev) => {
      const res = prev.map((_, i) => {
        if (i === index) {
          return (prev[i] = true);
        }
        return prev[i];
      });
      return [...res];
    });
  };

  window.addEventListener("scroll", handleScroll, true);
  return (
    <>
      <Wrapper ref={contentRef}>
        <Navigation backgroundColor={backgroundColor} ref={navRef}>
          <Back
            onClick={() => {
              navigate("/feed");
            }}
          />
        </Navigation>

        <Content>
          <ContentInner>
            {newImageData !== [] &&
              newImageData?.map((image, index) => {
                console.log(startAnimation, "here");
                return (
                  <ImageContainer
                    ref={imageRef[index]}
                    animated={startAnimation[index]}
                    key={index}
                  >
                    <Image key={index} src={image?.image?.url} alt="cat" />
                    <ImageOverlay
                      onClick={(e) => {
                        e.preventDefault();
                        handleAnimation(index);
                        if (done) {
                          const newResult = { ...newImageData[index] };
                          const newArray = newImageData.filter(
                            (image, index) => {
                              if (image.id !== newResult.id) {
                                return image;
                              }
                            }
                          );
                          // setStartAnimation((prev) => {
                          //   const res = prev.map((_, i) => {
                          //     if (i !== index) {
                          //       return (prev[i] = false);
                          //     }
                          //   });
                          //   return [...res];
                          // });

                          console.log(newArray, "newArray");

                          setNewImageData([...newArray]);
                          // if (newArray[index].favorite === false) {
                            // deleteFavorite(apiKey, image.id);
                          // }
                        }
                      }}
                    >
                      <Like liked={newImageData[index].favorite} />
                    </ImageOverlay>
                  </ImageContainer>
                );
              })}
          </ContentInner>
        </Content>
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  display: flex;
  width: 360px;
  height: 710px;
  flex-direction: column;
  overflow-y: scroll;
  overflow-x: hidden;
`;

const Navigation = styled.div`
  width: inherit;
  height: 8%;
  background-color: ${({ backgroundColor }) => backgroundColor};
  position: absolute;
  align-items: center;
  display: flex;
  z-index: 4;
`;

const Content = styled.div`
  width: 100%;
  height: 90%;
  margin-top: 65px;
`;

const ContentInner = styled.div``;

const SlideOut = keyframes`
  0% {
    transform: translateX(0);
  }
  20% {
    transform: translateX(-80px);
  }
  40% {
    transform: translateX(-160px);
  }
  60% {
    transform: translateX(-240px);
  }
  80% {
    transform: translateX(-320px);
  }
  100% {
    transform: translateX(-400px);
  }
`;

const ImageContainer = styled.div`
  position: relative;
  animation: ${({ animated }) =>
    animated &&
    css`
      ${SlideOut} 0.3s
    `};
  animation-delay: 2s;
`;

const Image = styled.img`
  width: 95%;
  height: auto;
`;

const ImageOverlay = styled.div`
  position: absolute;
  right: 20px;
  bottom: 50px;
  width: 34px;
  height: 34px;
  background-color: #62cc6d;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
`;

export default FavoriteScreen;
