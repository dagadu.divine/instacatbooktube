import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { css } from "styled-components";
import { keyframes } from "styled-components";
import { Book, Cat, Instagram, Youtube } from "../components/svgs";
import { useNavigate } from "react-router-dom";

//hooks
import { useAuth,useInstaCatBook } from "../hooks";

const LoginScreen = () => {
  let navigate = useNavigate();

  const { checkApiKey, apiKeyError, setApiKeyError, success } = useAuth();
  const {getAllImages} = useInstaCatBook();
  const [text, setText] = useState("");
  const [textComplete, setTextComplete] = useState();

  const pattern =
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

  const [disabled, setDisabled] = useState(true);

  const handleButtonClick = (e) => {
    e.preventDefault();
    setTextComplete(text);
    setDisabled(true);
    pattern.test(text) ? checkApiKey(text) : setApiKeyError(true);
  };

  useEffect(() => {
    if (success) {
      getAllImages();
    }
  },[success]);

  return (
    <>
      <Wrapper>
        <ImageHeader>
          <Instagram />
          <Cat />
        </ImageHeader>
        <ImageHeader>
          <Book />
          <Youtube />
        </ImageHeader>
        <TextInput
          error={apiKeyError}
          key={textComplete}
          placeholder="Enter your API key..."
          onChange={(e) => {
            setText(e.target.value);
            pattern.test(e.target.value)
              ? setDisabled(false)
              : setDisabled(true);
          }}
        />
        <Button disabled={disabled}>
          <ButtonText
            onClick={(e) => {
              handleButtonClick(e);
            }}
          >
            Sign In
          </ButtonText>
        </Button>
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  height: 710px;
  width: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  padding-top: 58px;
  display: flex;
  flex-direction: column;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const ImageHeader = styled.div`
  margin-top: 10px;
  padding-inline: 10px;
  width: 60%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  justify-self: center;
  align-self: center;
`;

const Shake = keyframes`
  0% {
    transform: translateX(0);
  }
  20% {
    transform: translateX(-10px);
  }
  40% {
    transform: translateX(10px);
  }
  60% {
    transform: translateX(-10px);
  }
  80% {
    transform: translateX(10px);
  }
  100% {
    transform: translateX(0);
  }
`;

const TextInput = styled.input`
  width: 90%;
  height: 40px;
  margin-top: 120px;
  align-self: center;
  font-size: 14px;
  border-radius: 4px;
  color: ${({ error }) => (error ? "#CC8862" : "#62CC6D")};
  border-color: ${({ error }) => (error ? "#CC8862" : "#62CC6D")};
  animation: ${({ error }) =>
    error &&
    css`
      ${Shake} 0.82s
    `};
  border-width: 1px;
`;

const Button = styled.button`
  width: 92%;
  height: 48px;
  margin-top: 20px;
  align-self: center;
  border-radius: 4px;
  background-color: ${({ disabled }) => (disabled ? "#858585" : "#62CC6D")};
  border-color: #858585;
`;

const ButtonText = styled.p`
  color: white;
`;

export default LoginScreen;
