import React, { useState, useEffect, createContext } from "react";
import instaCatBookService from "../service/instaCatBookService";

const InstaCatBookContext = createContext({});

const Provider = ({ children }) => {
  const [limit, setLimit] = useState(10);
  const [imagesData, setImagesData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [favoriteData, setFavoriteData] = useState([]);
  const [favoriteLimit, setFavoriteLimit] = useState(10);
  const [APIKey, setAPIKey] = useState("");

  useEffect(() => {
    if (APIKey !== "") {
      getAllImages();
    }
  }, [limit, APIKey]);

  useEffect(() => {
    if (APIKey !== "") {
      getAllFavorites();
    }
  }, [favoriteLimit, APIKey]);
  return (
    <InstaCatBookContext.Provider
      value={{
        getAllImages,
        imagesData,
        saveFavorite,
        deleteFavorite,
        getAllFavorites,
        favoriteData,
        setLimit,
      }}
    >
      {children}
    </InstaCatBookContext.Provider>
  );

  async function getAllImages(apiKey) {
    try {
      setLoading(true);
      const API_KEY = localStorage.getItem("API-key");
      setAPIKey(API_KEY);
      const { data } = await instaCatBookService.getAllImages(API_KEY, limit);
      if (data) {
        const results = data.map((obj) => ({ ...obj, favorite: false }));
        setImagesData((prevState) => [...prevState, ...results]);
      }
      setLoading(false);
    } catch (e) {}
  }

  async function saveFavorite(apiKey, payload) {
    try {
      const { data } = await instaCatBookService.saveFavorite(apiKey, payload);
      if (data) {
        setSuccess(true);
      }
    } catch (e) {}
  }

  async function deleteFavorite(apiKey, id) {
    try {
      const { data } = await instaCatBookService.deleteFavorite(apiKey, id);
      if (data) {
        setSuccess(true);
      }
    } catch (e) {}
  }

  async function getAllFavorites(apiKey) {
    try {
      const API_KEY = localStorage.getItem("API-key");
      setAPIKey(API_KEY);
      const { data } = await instaCatBookService.getAllFavorites(
        API_KEY,
        favoriteLimit
      );
      if (data) {
        const results = data.map((obj) => ({ ...obj, favorite: true }));
        setFavoriteData((prevState) => [
          ...new Set([...prevState, ...results]),
        ]);
      }
    } catch (e) {}
  }
};

InstaCatBookContext.ProviderWrapper = Provider;

export default InstaCatBookContext;
