import React, { useState, useEffect, createContext } from "react";
import { authService } from "../service";
import { useNavigate } from "react-router-dom";

const AuthContext = createContext({});

const Provider = ({ children }) => {
  let navigate = useNavigate();

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [apiKeyError, setApiKeyError] = useState(false);

  return (
    <AuthContext.Provider
      value={{
        checkApiKey,
        error,
        loading,
        success,
        apiKeyError,
        setApiKeyError,
      }}
    >
      {children}
    </AuthContext.Provider>
  );

  async function checkApiKey(apiKey) {
    try {
      setLoading(true);
      localStorage.setItem("API-key", apiKey);
      const data = await authService
        .authenticate(apiKey)
        .then()
        .catch((err) => {
          if (err) {
            setError(true);
            setSuccess(false);
            setApiKeyError(true);
          }
          return;
        });
      if (data) {
        console.log("running");
        setApiKeyError(false);
        setSuccess(true);
        setError(false);
        navigate("/feed");
      }
      setLoading(false);
    } catch (e) {
      console.log(e);
    }
  }
};

AuthContext.ProviderWrapper = Provider;

export default AuthContext;
