import { LoginPage, FeedPage, FavoritesPage } from "../pages";

const routes = [
  {
    path: "/",
    exact: true,
    component: LoginPage,
  },
  {
    path: "/feed",
    exact: true,
    component: FeedPage,
  },
  {
    path: "/favorites",
    exact: true,
    component: FavoritesPage,
  },
];

export default routes;
