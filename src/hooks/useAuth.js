import { useContext } from "react";

// Context
import { AuthContext } from "../context/";

const useAuth = () => {
  const authStore = useContext(AuthContext);

  return authStore;
};

export default useAuth;
