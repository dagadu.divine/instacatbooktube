import { useContext } from "react";

// Context
import { InstaCatBookContext } from "../context/";

const useInstaCatBook = () => {
  const instaCatBookStore = useContext(InstaCatBookContext);

  return instaCatBookStore;
};

export default useInstaCatBook;
