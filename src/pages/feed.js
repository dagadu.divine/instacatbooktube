import React, { Component, useEffect, useState } from "react";
import { Phone } from "../components/common";
import { FeedScreen } from "../screens";

import { useAuth } from "../hooks";

const FeedPage = () => {
  // const {} = useAuth();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    const API_KEY = localStorage.getItem("API-key");
    if (API_KEY === null) {
      window.location.href = "/";
    }
    setLoading(false);
  }, []);
  return (
    <>
      {loading ? (
        <div>Loading...</div>
      ) : (
        <Phone>
          <FeedScreen />
        </Phone>
      )}
    </>
  );
};

export default FeedPage;
