export { default as LoginPage } from "./login";
export { default as FeedPage } from "./feed";
export { default as FavoritesPage } from "./favorites";
export { default as NoMatch } from "./noMatch";
