import React, { Component } from "react";
import { Phone } from "../components/common";
import { LoginScreen } from "../screens";

const LoginPage = () => {
  return (
    <>
      <Phone>
        <LoginScreen />
      </Phone>
    </>
  );
};

export default LoginPage;
