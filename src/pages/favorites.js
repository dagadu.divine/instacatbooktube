import React, { Component, useEffect, useState } from "react";
import { Phone } from "../components/common";
import { FavoriteScreen } from "../screens";

const FavoritesPage = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    const API_KEY = localStorage.getItem("API-key");
    if (API_KEY === null) {
      window.location.href = "/";
    }
    setLoading(false);
  }, []);
  return (
    <>
      <Phone>
        <FavoriteScreen />
      </Phone>
    </>
  );
};

export default FavoritesPage;
