export { default as Instagram } from "./instagram";
export { default as Cat } from "./cat";
export { default as Book } from "./book";
export { default as Youtube } from "./youtube";
export { default as Exit } from "./exit";
export { default as Favorites } from "./favorites";
export { default as Like } from "./like";
export { default as Back } from "./back";
