import React from "react";
import styled from "styled-components";

const Phone = ({ children, phonebg }) => {
  return (
    <Holder>
      <BodyOne>
        <PhoneBackground>
          <MainHolder phonebg={phonebg}>{children}</MainHolder>
        </PhoneBackground>
      </BodyOne>
    </Holder>
  );
};

const Holder = styled.div`
  display: grid;
  height: 100vh;
  width: 100vw;
  align-content: center;
  justify-content: center;
  background-color: gray;
`;

const BodyOne = styled.div`
  display: grid;
  height: 710px;
  width: 360px;
  background-color: #000000;
  border-radius: 30px;
  align-self: center;
  @media screen and (max-width: 480px) {
    background-color: transparent;
    width: 100vw;
    height: 100vh;
  }
`;

const PhoneBackground = styled.div`
  display: grid;
  border-radius: 30px;
  background-color: #ffffff;
  @media screen and (max-width: 480px) {
    border-radius: 0px;
  }
`;

const MainHolder = styled.div`
  display: grid;
  align-content: flex-start;
  background-color: ${({ phonebg }) => (phonebg ? phonebg : `#f2f2f2`)};
  overflow: hidden;
  @media screen and (max-width: 480px) {
    border-radius: 0px;
    padding-bottom: 5px;
  }
`;

export default Phone;

Phone.propTypes = {};
